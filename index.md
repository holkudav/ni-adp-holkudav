##ADP Semestral Work
The task was to create a simple game and in the process use as many design pattern as possible, at least 10.

I've created a platformer where the goal is to reach the _goal door_. The task seems to be simple, but it is not. 
In order to achieve the goal you have to cooperate with your _past self_.

###The controlls
  - Jump: __Arrow Up__ 
  - Left: __Arrow Left__ 
  - Right: __Arrow Right__
  - Spawn new clone: __L__


##Used Patterns

###Singleton
History, Input, Collisions ...

I have used this pattern a lot trough out the project.
I am aware that in some cases this pattern is considered more as anti-pattern, but I believe that all my usages are justified.

###Observer
Audio

I use observer pattern for audio. ```CharacterState``` is the subject and ```Audio``` is the observer.
For now the subject updates the observers whenever a character jumps or dies.
The only observer is ```Audio``` now, but thanks to this pattern I can easily add other observers (screen shake when a character dies for example).


###Command
Character dying

When a snake touches a character, it sends him ```DieCommand```.
On the start of its update loop, character processes the event.
This pattern is really cool. My use-case however is not that great for it.
It would be more useful with wider variety of commands or for stashing multiple commands in a queue.
I have it implemented with the queue, but again, the use-case is not perfect.

###State
SnakeState

Snake has two states.
 1. Idle
 2. Chasing

When idle, the snake _walks_ from left to right and back.
When in chasing mode, the snake always _walks_ to the closest alive character.

###Adapter
Keyboard to UserInputInterface via KeyboardAdapter

I have an ```UserInput``` interface with 4 methods
  - getMoveLeft(): boolean
  - getMoveRight(): boolean
  - getJump(): boolean
  - getClone(): boolean

All player actions needs to be mapped to this interface.
Now the game can be controlled only by the keyboard. 
The ```Keyboard``` class however has completely different interface.
It stores codes of all currently pressed keys and offer single method ```    isPressed(keyCode: string): boolean```.

Using the ```KeyboardAdapter``` I can map the ```Keyboard``` interface to ```UserInput``` interface.
The adapter I am using treats arrow keys as the movement keys and L as the clone key.
There is nothing stopping  me from adding different keyboard adapter that uses WSAD instead.

Having the ```UserInput``` interface separate enables me also to easily add support for touch screen's or console's controllers.

###Facade
Input

Facade is a class that simplify interface of some other class(es) and provides this simpler interface to the program without blocking the access to the classes that are being simplified.

I think I am using the facade, but maybe I am fetching the definition little too far.
I imagine my ```Input``` class as a facade to ```UserInput```. It simplifies the interface, and offers ```snapshot``` and ```default``` methods.
Snapshot is the current state of the user input. Default is user input when no keys are pressed.

I have (consciously) used 11 patterns just in case this is not proper facade pattern. 


###Strategy
Camera movement

I use different camera movements (strategies) for each level.
The caller always calls its ```update``` method. 
The behavior is based on the currently selected strategy.

###Decorator 
Camera movement algorithms

Currently, I have ```Following```, ```Snapping```, ```HorizontalyFollowing``` and ```Shaking``` strategies for the camera movement.

They are a combination of ```CameraAlgorithm``` decorators.
Currently, I have these
 - FollowingHorizontally
 - FollowingVertically
 - SnappingHorizontally
 - SnappingVertically
 - ShakingHorizontally
 - ShakingVertically
 - WorldConstrained

It is very easy to add new decorators with new behavior. 
The strategies are just a combination of these decorators.
I already have more possible combinations than I am currently using.

###Builder
Construction of camera algorithms.

And to use those CameraAlgorithm decorators easily I created a ```CameraAlgorithmBuilder```.
The solution is really elegant and easy to use, I'll certainly use these patterns again.
The end result (Decorators + Builder) looks like this.

    HorizontallyFollowing: new CameraAlgorithmBuilder()
            .withHorizontalFollowing(3) // speed
            .withVerticalSnapping(20)   // tolerance
            .constrainedToWorldCoordinates()
            .build(),

###Servant
Collisions

A servant is an object used to offer some functionality to a group of classes without defining that functionality in each of them.

```Collisions``` is my servant object that provides collision calculating functionality to all subclasses of WorldObject (Basically everything on the screen)

Before I get to know this pattern, this was my code

    Collisions.Instance.areColliding(objectA, objectB)

With the servant pattern, the code now looks like this

    objectA.isCollidingWith(objectB)

The difference is subtle, but the benefits are clear.

 1. Looks more like object-oriented programming
 2. The code is more readable
 3. Every ```WorldObject``` now has the collisions' functionality packed inside it. I don't have to remember to use other class for collisions.

###Factory
ComponentsFactory

Factory pattern is here to replace the ```new``` keyword because ```new is glue```.
When we use factory instead of ```new``` and we need to switch to a different class later, we change it only on one place, inside the factory (or we create a new Factory).

I understand the concept and this is a very famous pattern, but I have never felt the need to use it.
I have to use 10 patterns so I forced myself to use it.
I've created ```ComponentsFactory``` interface and ```LevelComponentsFactory``` that implements it.

    export interface ComponentsFactory {
        getStateComponent(state: GameState): GameComponent
        getBackground(state: GameState): GameComponent
        getPlatform(state: GameState): GameComponent
        getCoins(state: GameState): GameComponent
        getGoal(state: GameState): GameComponent
        getSpawnPlaces(state: GameState): GameComponent
        getCharacters(state: GameState): GameComponent
        getLava(state: GameState): GameComponent
        getSnakes(state: GameState): GameComponent
    }

It creates game components (Stateless parts of everything seen on the screen).

I am not 100% sure I've used the pattern correctly. The factory should produce products. Should they be different? I mean should they implement different interfaces?
I struggle to justify its usage here. Maybe I should have had abstract class ```ComponentFactory``` and many concrete implementations of it (```BackgroundFactory```, ```PlatformFactory```...)

So yes, I am not sure this one is correct, but hopefully with the facade it's 10 :)

##I may have used other patterns
This game is also for the NI-APH subject. I've later read on teams that we are allowed to use NI-APH semestral work for this subject as well.
At first I did not care about the patterns in this project at all and I've started adding them later using a lot of refactoring.

When I was reading about all the patterns and saw the examples, I noticed that I may be using some of them already. 
They don't exactly follow the pattern, but the core idea is there I think.


##Tests
For testing I am using Karma with Jasmine. I have 11 tests, which is more than required 5.

I test weather players coordinates are updating as they should based on the user input and collisions.
This I test, because it is the core of the game.

Next I test camera movement, because it was written later with the patters in mind and it's really easy to test :)

We were required to use mocking in the tests. In the character movement tests I have a mocked character input and in the camera tests I spy on the viewport instance.

## Extra features
This is hard to judge because I don't have the _angy birds_ game...
Anyway I'll list some features that I think are something extra.

 1. __Cloning__ This is the core mechanic of the game. The clones repeat your previous actions. You have to cooperate with them and also be careful not to mess up their path. I was hoping to use memento pattern for this, but it was not suitable for it.
 2. __Enemies__ There is a snake which chase and kills the player and it's clones. This opens up new game mechanics. The player needs to use one clone as a lure so the other can pass trough.
 3. __Coins__ There are collectable coins in the game. Currently they don't do anything, but ye.. they are there :)
 4. __Levels__ I have created 5 levels so far. The main beauty is that they are almost entirely described in generated json. I have just a little additional information in the code (see ```maps.ts```) regarding the camera. This means that
    1. The code is clean, and separate from the level design and tools used for it.
    2. I can use the same level layout (json) with different camera configuration (the code) to force a different gameplay in the same level.
 5. __Sound__ I know it's very simple sound but there was nothing about sound in the requirements, so I guess it's extra
 6. __Menus__ You can choose a level to play

There are also many unfinished features which I plan to include for the NI-APH which has much more generous deadline.
The main think that is needed is to fix the collisions. They are not reliable and they can crash the hole game. 
I have already spent too much time on that, but it's still very bad.

Features that are in progress is Lava and Snake Hunter (which projectiles kill also the player).

## How to run

 1. Download the source code.
 2. ```npm install```
 3. ```npm start```

To run the tests run ```npm test``` instead of ```npm start```

###Note
I've followed first 3 parts of this tutorial https://medium.com/javascript-in-plain-english/platform-game-with-pixijs-part-1-ca2ed93c0808
Therefore if you see a different autohor somewhere, that is the reason.
However, if you would compare the repository from the tutorial with this repository, you would see there is almost nothing in common anymore.

I almost regret building on top of that repository, because I have to carry some design decisions I don't like.
