import {Viewport} from "pixi-viewport";
import {CameraAlgorithmBuilder} from "../src/state/camera/algorithms/cameraAlgorithmBuilder";
import {CharacterState} from "../src/state/characterState";

const viewport = new Viewport({
        screenWidth: 5,
        screenHeight: 3,
        worldWidth: 10,
        worldHeight: 11,
    },
)

describe('Test camera movement algorithms', () => {
    it('Should not snap before limit', () => {
        spyOn(viewport, 'moveCorner')

        const algo = new CameraAlgorithmBuilder()
            .withHorizontalSnapping()
            .withVerticalSnapping()
            .build()

        algo.updateViewpoint(viewport, new CharacterState({x: 4, y: 3}))

        expect(viewport.moveCorner).not.toHaveBeenCalled()
    })

    it('Should snap when out of screen horizontally', () => {
        spyOn(viewport, 'moveCorner')

        const algo = new CameraAlgorithmBuilder()
            .withHorizontalSnapping()
            .withVerticalSnapping()
            .build()

        algo.updateViewpoint(viewport, new CharacterState({x: 6, y: 3}))

        expect(viewport.moveCorner).toHaveBeenCalled()
    })

    it('Should snap when out of screen vertically', () => {
        spyOn(viewport, 'moveCorner')

        const algo = new CameraAlgorithmBuilder()
            .withHorizontalSnapping()
            .withVerticalSnapping()
            .build()

        algo.updateViewpoint(viewport, new CharacterState({x: 4, y: 5}))

        expect(viewport.moveCorner).toHaveBeenCalled()
    })

    it('Should move towards the character horizontally', () => {
        spyOn(viewport, 'moveCenter')

        const vh = 3

        const algo = new CameraAlgorithmBuilder()
            .withHorizontalFollowing(vh)
            .build()

        algo.updateViewpoint(viewport, new CharacterState({x: 100, y: 100}))

        expect(viewport.moveCenter).toHaveBeenCalledWith(viewport.center.x + vh, viewport.center.y)
    })

    it('Should move towards the character vertically', () => {
        spyOn(viewport, 'moveCenter')

        const vv = 1

        const algo = new CameraAlgorithmBuilder()
            .withVerticalFollowing(vv)
            .build()

        algo.updateViewpoint(viewport, new CharacterState({x: 100, y: 100}))

        expect(viewport.moveCenter).toHaveBeenCalledWith(viewport.center.x, viewport.center.y + vv)
    })

})
