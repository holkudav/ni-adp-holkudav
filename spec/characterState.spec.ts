import {UserInput} from "../src/framework/inputs/userInput";
import {Input} from "../src/framework/inputs/input";
import {CharacterCollisions, Collision} from "../src/framework/collisions/model";
import {CharacterMode, World} from "../src/constants/constants";
import {CharacterState} from "../src/state/characterState";

class MockInput implements UserInput{
    constructor(
        public left: boolean,
        public right: boolean,
        public jump: boolean,
        public clone: boolean,
        public pause: boolean
    ) { }

    getClone(): boolean {
        return this.clone;
    }

    getExit(): boolean {
        return this.pause;
    }

    getJump(): boolean {
        return this.jump;
    }

    getMoveLeft(): boolean {
        return this.left;
    }

    getMoveRight(): boolean {
        return this.right;
    }
}

describe('Character state tests', function() {
    it ('Should work', () => {
        const input = new MockInput(true, false, false, false, true)
        Input.Instace.getDefault()
        new CharacterCollisions(new Collision(), new Collision())
        World.Character.Speed
        CharacterMode.Dead
            // const state = new CharacterState({})
        expect(input).toBeTruthy()
    })

    it('Should move only by gravity', function() {
        const state = new CharacterState({x: 10, y: 9})
        expect(state.x).toBe(10)
        expect(state.y).toBe(9)

        state.update(Input.Instace.getDefault(), new CharacterCollisions(new Collision(), new Collision()))

        expect(state.x).toBe(10)
        expect(state.y).toBe(9 + World.Gravity)
    });

    it('Should move by gravity and collisions', function() {
        const state = new CharacterState({x: 10, y: 9})
        expect(state.x).toBe(10)
        expect(state.y).toBe(9)

        state.update(Input.Instace.getDefault(), new CharacterCollisions(new Collision(1, 2), new Collision(3, 4)))

        expect(state.x).toBe(10 + 2 + 4)
        expect(state.y).toBe(9 + World.Gravity + 1 + 3)
    });

    it('Should move by gravity and user input', function() {
        const state = new CharacterState({x: 10, y: 9})
        expect(state.x).toBe(10)
        expect(state.y).toBe(9)

        state.update(new MockInput(true, false, true, false, true), new CharacterCollisions(new Collision(), new Collision()))

        expect(state.x).toBe(10 - World.Character.Speed)
        expect(state.y).toBe(9 + World.Gravity)
    });

    it('Should move by gravity, user input and collisions', function() {
        const state = new CharacterState({x: 10, y: 9})
        expect(state.x).toBe(10)
        expect(state.y).toBe(9)

        state.update(new MockInput(false, true, false, true, false), new CharacterCollisions(new Collision(3, 4), new Collision(1, 2)))

        expect(state.x).toBe(10 + World.Character.Speed + 4 + 2)
        expect(state.y).toBe(9 + World.Gravity + 3 + 1)
    });

    it('Should ignore user inputs when dead', function() {
        const state = new CharacterState({x: 10, y: 9})
        state.mode = CharacterMode.Dead

        expect(state.x).toBe(10)
        expect(state.y).toBe(9)
        expect(state.mode).toBe(CharacterMode.Dead)

        state.update(new MockInput(false, true, false, true, true), new CharacterCollisions(new Collision(3, 4), new Collision(1, 2)))

        expect(state.x).toBe(10)
        expect(state.y).toBe(9 + World.Gravity + 3 + 1)
    });
});
