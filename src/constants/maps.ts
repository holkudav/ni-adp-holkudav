import {Asset, createLevel, ParsedTile} from "../framework/level";
import {CameraMode} from "../state/camera/camera";

export class LevelsManager {
    static get numberOfLevels() {
        return levelExtraData.length
    }

    static async getLevel(levelNumber: number): Promise<Level> {
        return {
            ...createLevel(await mapLoaders[levelNumber - 1]() as any),
            ...levelExtraData[levelNumber -1]
        }
    }

    static levelNumber = 5
}

const levelExtraData = [
    {
        levelNumber: 1,
        viewWidth: 20,
        viewHeight: 10,
        camera: CameraMode.Snapping,
    },
    {
        levelNumber: 2,
        viewWidth: 20,
        viewHeight: 10,
        camera: CameraMode.Snapping,
    },
    {
        levelNumber: 3,
        viewWidth: 20,
        viewHeight: 10,
        camera: CameraMode.HorizontallyFollowing,
    },{
        levelNumber: 4,
        viewWidth: 20,
        viewHeight: 10,
        camera: CameraMode.Snapping,
    },
    {
        levelNumber: 5,
        viewWidth: 20,
        viewHeight: 10,
        camera: CameraMode.Following,
    },
]

const mapLoaders = [
    () => import('../assets/levels/level1.json'),
    () => import('../assets/levels/level2.json'),
    () => import('../assets/levels/level3.json'),
    () => import('../assets/levels/level4.json'),
    () => import('../assets/levels/level5.json'),
]

export interface Level {
    levelNumber: number
    levelWidth: number
    levelHeight: number
    viewWidth: number
    viewHeight: number
    tileWidth: number
    tileHeight: number
    camera: CameraMode
    spawnPlaces: ParsedTile[]
    tiles: ParsedTile[]
    lavaTiles: ParsedTile[]
    coins: Asset[]
    snakes: Asset[]
    goalDoor: Asset
}
