
export const Textures = {
  Platform: "assets/platform/Tileset.json",
  Lava: "assets/platform/lava.json",
  SpawnPlaces: "assets/platform/spawn_places/spawnPlaces.json",
  Decorations: "assets/platform/decorations.json",
  Character: "assets/characters/king.json",
  SnakeHunter: "assets/characters/boy.json",
  Snake: "assets/characters/snake.json",
  PastCharacter: "assets/characters/kingDesaturated.json",
  GoalDoor: 'assets/miscellaneous/door/door.json',
  Coin: 'assets/miscellaneous/coin/coin.json',
  Button: 'assets/miscellaneous/button/button.json',
  Lever: 'assets/miscellaneous/lever/lever.json',
  Background: 'assets/background/Background.json'
};

export const World = {
  Character: {
    Speed: 4,
    JumpSpeed: 10,
    JumpThreshold: 230
  },
  Snake: {
    Speed: 1,
    WalkThreshold: 256,   // tiles
    FollowDistance: 700,
  },
  SnakeHunter: {
    Speed: 3,
    JumpSpeed: 8,
    JumpThreshold: 230
  },
  Gravity: 10
};

export const HorizontalDirections = {
  Left: -1,
  Right: 1
};

export enum CharacterMode {
  Jumping = 'Jumping' ,
  Running = 'Running',
  Falling = 'Falling',
  Idle = 'Idle',
  Dead = 'Dead',
}
