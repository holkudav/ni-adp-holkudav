import {Level} from "../constants/maps";
import {History} from "./history";
import {CharacterState} from "./characterState";
import {CoinState} from "./coinState";
import {SnakeState} from "./snakeState";
import {CharacterAudio} from "../framework/audio";
import {GoalState} from "./goalState";

export interface GameState {
    level: Level;
    states: {
        characters: CharacterState[],
        snakes: SnakeState[],
        coins: CoinState[],
        goal: GoalState,
    };
}

export function initState(level: Level): GameState {
    const characters: CharacterState[] = []
    for (let i = 0; i <= History.Instance.numberOfPastCharacters; i++) {
        characters.push(CharacterState.init(level, i))
    }
    const coins = level.coins.map(it => CoinState.init(it))
    const snakes = level.snakes.map(it => SnakeState.init(it))
    const goal = GoalState.init(level.goalDoor)

    CharacterAudio.attachTo(characters)

    return {
        level,
        states: {
            characters: characters,
            coins,
            snakes,
            goal
        }
    }
}
