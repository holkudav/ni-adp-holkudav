import {WorldObject} from "../worldObject";
import {Viewport} from "pixi-viewport";
import {CameraAlgorithmBuilder} from "./algorithms/cameraAlgorithmBuilder";

export enum CameraMode {
    Following = 'Following',
    Snapping = 'Snapping',
    HorizontallyFollowing = 'HorizontallyFollowing',
    Shaking = 'Shaking',
}

export class Camera {
    private static instance = new Camera()
    private viewport: Viewport
    private mode: CameraMode = CameraMode.Snapping
    private strategies = {
        Following: new CameraAlgorithmBuilder()
            .withHorizontalFollowing(3)
            .withVerticalFollowing(3)
            .constrainedToWorldCoordinates()
            .build(),
        Snapping: new CameraAlgorithmBuilder()
            .withHorizontalSnapping(20)
            .withVerticalSnapping(20)
            .constrainedToWorldCoordinates()
            .build(),
        HorizontallyFollowing: new CameraAlgorithmBuilder()
            .withHorizontalFollowing(3)
            .withVerticalSnapping(20)
            .constrainedToWorldCoordinates()
            .build(),
        Shaking: new CameraAlgorithmBuilder()
            .withHorizontalShaking(8)
            .withVerticalShaking(8)
            .constrainedToWorldCoordinates()
            .build()
    }
    static get Instance() {
        return this.instance
    }

    update(target: WorldObject) {
        this.strategies[this.mode].updateViewpoint(this.viewport, target)
    }

    setMode(mode: CameraMode) {
        this.mode = mode
    }

    setViewPort(viewport: Viewport) {
        this.viewport = viewport
    }

    getViewPort() {
        return this.viewport
    }

    moveCornerTo(x: number, y: number) {
        this.viewport.moveCorner(x, y)
    }
}
