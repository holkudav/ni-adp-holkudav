import {CameraAlgorithm} from "../cameraAlgorithm";
import {WorldObject} from "../../../worldObject";
import {Viewport} from "pixi-viewport";

export class SnappingVertically implements CameraAlgorithm {

    constructor(private decoratedAlgo: CameraAlgorithm, private tolerance: number) { }

    updateViewpoint(viewport: Viewport, target: WorldObject): void {
        this.decoratedAlgo.updateViewpoint(viewport, target)
        this.snap(viewport, target)
    }

    private snap(viewport: Viewport, target: WorldObject) {
        if (target.y - this.tolerance > viewport.bottom) {
            viewport.moveCorner(viewport.left, viewport.bottom)
        }
        else if (target.y + this.tolerance < viewport.top) {
            viewport.moveCorner(viewport.left, viewport.top - viewport.worldScreenHeight)
        }
    }
}
