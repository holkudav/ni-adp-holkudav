import {CameraAlgorithm} from "../cameraAlgorithm";
import {WorldObject} from "../../../worldObject";
import {Viewport} from "pixi-viewport";

export class SnappingHorizontally implements CameraAlgorithm {

    constructor(private decoratedAlgo: CameraAlgorithm, private tolerance: number) { }

    updateViewpoint(viewport: Viewport, target: WorldObject): void {
        this.decoratedAlgo.updateViewpoint(viewport, target)
        this.snap(viewport, target)
    }

    private snap(viewport: Viewport, target: WorldObject) {
        if (target.x - this.tolerance > viewport.right) {
            viewport.moveCorner(viewport.right + 1, viewport.top)
        }
        else if (target.x + this.tolerance < viewport.left) {
            viewport.moveCorner(viewport.left - viewport.worldScreenWidth, viewport.top)
        }
    }
}
