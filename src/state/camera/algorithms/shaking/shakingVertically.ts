import {CameraAlgorithm} from "../cameraAlgorithm";
import {Viewport} from "pixi-viewport";
import {WorldObject} from "../../../worldObject";

export class ShakingVertically implements CameraAlgorithm {

    constructor(private decoratedAlgo: CameraAlgorithm, private amount: number) { }

    updateViewpoint(viewport: Viewport, target: WorldObject): void {
        this.decoratedAlgo.updateViewpoint(viewport, target)
        this.shake(viewport)
    }

    private shake(viewport: Viewport) {
        viewport.moveCorner(
            viewport.corner.x,
            viewport.corner.y + this.getNumber()
        )
    }

    private getNumber() {
        return Math.random() * this.amount - this.amount / 2
    }
}
