import {WorldObject} from "../../worldObject";
import {Viewport} from "pixi-viewport";

export interface CameraAlgorithm {
    updateViewpoint(viewport: Viewport, target: WorldObject): void
}

export class BasicCameraAlgorithm implements CameraAlgorithm {
    updateViewpoint(viewport: Viewport, target: WorldObject) { }
}
