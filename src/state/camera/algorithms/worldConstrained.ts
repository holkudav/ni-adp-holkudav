import {CameraAlgorithm} from "./cameraAlgorithm";
import {WorldObject} from "../../worldObject";
import {Viewport} from "pixi-viewport";

export class WorldConstrained implements CameraAlgorithm {
    constructor(private decoratedAlgo: CameraAlgorithm) { }

    updateViewpoint(viewport: Viewport, target: WorldObject): void {
        this.decoratedAlgo.updateViewpoint(viewport, target)
        this.constraint(viewport)
    }

    private constraint(viewport: Viewport) {
        if (viewport.corner.x < 0) {
            viewport.moveCorner(0, viewport.corner.y)
        } else if (viewport.corner.x > viewport.worldWidth - viewport.worldScreenWidth) {
            viewport.moveCorner(viewport.worldWidth - viewport.worldScreenWidth, viewport.corner.y)
        }

        if (viewport.corner.y < 0) {
            viewport.moveCorner(viewport.corner.x, 0)
        } else if (viewport.corner.y > viewport.worldHeight - viewport.worldScreenHeight) {
            viewport.moveCorner(viewport.corner.x, viewport.worldHeight - viewport.worldScreenHeight)
        }
    }
}
