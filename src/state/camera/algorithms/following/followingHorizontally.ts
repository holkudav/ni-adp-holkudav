import {CameraAlgorithm} from "../cameraAlgorithm";
import {Viewport} from "pixi-viewport";
import {WorldObject} from "../../../worldObject";

export class FollowingHorizontally implements CameraAlgorithm {

    constructor(private decoratedAlgo: CameraAlgorithm, private speed: number) { }

    updateViewpoint(viewport: Viewport, target: WorldObject): void {
        this.decoratedAlgo.updateViewpoint(viewport, target)
        this.moveTowardsTarget(viewport, target)
    }

    private moveTowardsTarget(viewport: Viewport, target: WorldObject) {
        let viewportX = viewport.center.x
        const targetX = target.x

        if (targetX > viewportX) {
            viewportX = Math.min(targetX, viewportX + this.speed)
        } else {
            viewportX = Math.max(targetX, viewportX - this.speed)
        }

        viewport.moveCenter(viewportX, viewport.center.y)
    }
}
