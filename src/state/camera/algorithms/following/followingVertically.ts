import {CameraAlgorithm} from "../cameraAlgorithm";
import {Viewport} from "pixi-viewport";
import {WorldObject} from "../../../worldObject";

export class FollowingVertically implements CameraAlgorithm {

    constructor(private decoratedAlgo: CameraAlgorithm, private speed: number) { }

    updateViewpoint(viewport: Viewport, target: WorldObject): void {
        this.decoratedAlgo.updateViewpoint(viewport, target)
        this.moveTowardsTarget(viewport, target)
    }

    private moveTowardsTarget(viewport: Viewport, target: WorldObject) {
        let viewportY = viewport.center.y
        const targetY = target.y

        if (targetY > viewportY) {
            viewportY = Math.min(targetY, viewportY + this.speed)
        } else {
            viewportY = Math.max(targetY, viewportY - this.speed)
        }

        viewport.moveCenter(viewport.center.x, viewportY)
    }
}
