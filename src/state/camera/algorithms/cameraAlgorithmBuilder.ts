import {BasicCameraAlgorithm, CameraAlgorithm} from "./cameraAlgorithm";
import {WorldConstrained} from "./worldConstrained";
import {FollowingHorizontally} from "./following/followingHorizontally";
import {SnappingHorizontally} from "./snapping/snappingHorizontally";
import {FollowingVertically} from "./following/followingVertically";
import {SnappingVertically} from "./snapping/snappingVertically";
import {ShakingVertically} from "./shaking/shakingVertically";
import {ShakingHorizontally} from "./shaking/shakingHorizontally";

export interface ICameraAlgorithmBuilder {
    withHorizontalFollowing(speed?: number): ICameraAlgorithmBuilder
    withVerticalFollowing(speed?: number): ICameraAlgorithmBuilder
    withHorizontalSnapping(tolerance?: number): ICameraAlgorithmBuilder
    withVerticalSnapping(tolerance?: number): ICameraAlgorithmBuilder
    withHorizontalShaking(amount?: number): ICameraAlgorithmBuilder
    withVerticalShaking(amount?: number): ICameraAlgorithmBuilder
    constrainedToWorldCoordinates(): ICameraAlgorithmBuilder

    build(): CameraAlgorithm
}

export class CameraAlgorithmBuilder implements ICameraAlgorithmBuilder {
    private cameraAlgo: CameraAlgorithm = new BasicCameraAlgorithm() 
    
    build(): CameraAlgorithm {
        return this.cameraAlgo;
    }

    constrainedToWorldCoordinates(): ICameraAlgorithmBuilder {
        this.cameraAlgo = new WorldConstrained(this.cameraAlgo)
        return this;
    }

    withHorizontalFollowing(speed: number = 0): ICameraAlgorithmBuilder {
        this.cameraAlgo = new FollowingHorizontally(this.cameraAlgo, speed)
        return this;
    }

    withVerticalFollowing(speed: number = 0): ICameraAlgorithmBuilder {
        this.cameraAlgo = new FollowingVertically(this.cameraAlgo, speed)
        return this;
    }

    withHorizontalSnapping(tolerance: number = 0): ICameraAlgorithmBuilder {
        this.cameraAlgo = new SnappingHorizontally(this.cameraAlgo, tolerance)
        return this;
    }

    withVerticalSnapping(tolerance: number = 0): ICameraAlgorithmBuilder {
        this.cameraAlgo = new SnappingVertically(this.cameraAlgo, tolerance)
        return this;
    }

    withHorizontalShaking(amount: number = 0): ICameraAlgorithmBuilder {
        this.cameraAlgo = new ShakingHorizontally(this.cameraAlgo, amount)
        return this;
    }

    withVerticalShaking(amount: number = 0): ICameraAlgorithmBuilder {
        this.cameraAlgo = new ShakingVertically(this.cameraAlgo, amount)
        return this;
    }
}
