import {UserInput} from "../framework/inputs/userInput";
import {Input} from "../framework/inputs/input";

export class History {
    private static instance = new History()
    
    private actions: UserInput[][]
    private actionsIndex: number
    
    static get Instance() {
        return this.instance
    }
    private constructor() { }
    
    initializeLevel() {
        this.actions = []
        this.actions.push([])
        this.clearPreviousIteration()
    }

    initializeIteration() {
        this.actions.push([])
        this.clearPreviousIteration()
    }

    tick() {
        this.actionsIndex ++
    }

    getActions(cloneNumber: number): UserInput {
        return this.actions[cloneNumber][this.actionsIndex] || Input.Instace.getDefault()
    }

    get numberOfPastCharacters() {
        return this.actions.length -1 // last is active and not past
    }

    recordActions(userInput: UserInput) {
        this.actions[this.numberOfPastCharacters].push(userInput)
    }

    private clearPreviousIteration() {
        this.actionsIndex = -1
    }
}

