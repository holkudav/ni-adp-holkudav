import {Collision, CollisionRect} from "../framework/collisions/model";
import {Collisions} from "../framework/collisions/collisions";

export abstract class WorldObject {
    x: number;
    y: number;
    vX: number;
    vY: number;

    abstract get collisionBox(): CollisionRect
    abstract get predictedVX(): number
    abstract get predictedVY(): number

    isCollidingWith(other: WorldObject): boolean {
        const collision = this.collisionWith(other)
        return collision.vertical !== 0 || collision.horizontal !== 0
    }

    collisionWith(other: WorldObject): Collision {
        return new Collision(
            Collisions.Instance.getVerticalCollision(this, other),
            Collisions.Instance.getHorizontalCollision(this, other),
            Collisions.Instance.getDiagonalCollision(this, other),
        );
    }
}
