import {WorldObject} from "./worldObject";
import {CollisionRect} from "../framework/collisions/model";
import {Collisions} from "../framework/collisions/collisions";
import {CharacterState} from "./characterState";
import {Asset} from "../framework/level";
import {newLevel} from "../index";
import {LevelsManager} from "../constants/maps";

export class GoalState extends WorldObject {
    private reached = false

    get collisionBox(): CollisionRect {
        return Collisions.Instance.createCornerCollisionBox(this, this.width / 2 , this.height / 2 )
    }

    get predictedVX(): number {
        return 0;
    }

    get predictedVY(): number {
        return 0;
    }

    constructor(
        x: number,
        y: number,
        public width: number,
        public height: number,
        public pickedUp: boolean = false
    ) {
        super();
        this.x = x;
        this.y = y
    }

    async update(characters: CharacterState[]) {
        if (this.reached) {
            return
        }
        if (characters.some(it => this.isCollidingWith(it))) {
            console.log('colliding')
            this.reached = true
            this.startNewLevel()
        }
    }

    private async startNewLevel() {
        LevelsManager.levelNumber ++
        await newLevel(LevelsManager.levelNumber)
    }

    static init(it: Asset) {
        return new GoalState(it.x, it.y, it.width, it.height)
    }
}
