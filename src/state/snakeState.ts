import {CharacterState} from "./characterState";
import {WorldObject} from "./worldObject";
import {DieCommand} from "../framework/commands";
import {CollisionRect} from "../framework/collisions/model";
import {Collisions} from "../framework/collisions/collisions";
import {CharacterMode, HorizontalDirections, World} from "../constants/constants";
import {Asset, ParsedTile} from "../framework/level";

export class SnakeState extends WorldObject {
    direction = -1;
    private idleState = new IdleState()
    private chasingState = new ChaseState()
    private state: State

    get collisionBox(): CollisionRect {
        return Collisions.Instance.createCenterCollisionBox(this, this.width, this.height)
    }
    private get visionBox(): CollisionRect {
        return Collisions.Instance.createCenterCollisionBox(
            this, World.Snake.FollowDistance, 256
        )
    }

    get predictedVX(): number {
        return this.direction * World.Snake.Speed;
    }

    get predictedVY(): number {
        return World.Gravity;
    }

    constructor(x: number, y: number, public width: number, public height: number) {
        super();
        this.x = x;
        this.y = y;
        this.vX = 0;
        this.vY = 0;
        this.setChasing(false)
    }

    static init(asset: Asset) {
        return new SnakeState(asset.x, asset.y, asset.width / 2, asset.height / 2)
    }

    update(characters: CharacterState[], tiles: ParsedTile[]) {
        const platformCollisions = Collisions.Instance.getCollisionWithNonMovables(this, tiles)
        this.vY = World.Gravity + platformCollisions.vertical
        this.vX = World.Snake.Speed * this.direction + platformCollisions.horizontal

        const characterToFollow = characters
            .filter(it => it.mode !== CharacterMode.Dead)
            .filter(it => { // all characters in snake's 'field of view'
                return Collisions.Instance.areColliding(it.collisionBox, this.visionBox)
            })
            .sort((a, b) => {
                return Math.abs(a.x - this.x) > Math.abs(b.x - this.x) ? 1 : 0
            })[0]

        this.state.walk(this, characterToFollow)
    }

    move() {
        this.x += this.vX
        this.y += this.vY
    }

    setChasing(chasing: boolean) {
        this.state = chasing
            ? this.chasingState
            : this.idleState
    }
}

interface State {
    walk(snake: SnakeState, characterToChaice: CharacterState): void
}

class IdleState implements State {
    private threshold = World.Snake.WalkThreshold
    private walkDistance = this.threshold / 2

    walk(snake: SnakeState, characterToChaice: CharacterState) {
        if (characterToChaice) {
            snake.setChasing(true)
        } else {
            this.walkDistance += World.Snake.Speed
            if (this.walkDistance > this.threshold) {
                snake.direction = - snake.direction
                this.walkDistance = 0
            }
            snake.move()
        }
    }
}

class ChaseState implements State {
    walk(snake: SnakeState, characterToChase: CharacterState): void {
        if (!characterToChase) {
            snake.setChasing(false)
        } else if (snake.isCollidingWith(characterToChase)) {
            characterToChase.enqueueCommand(new DieCommand(characterToChase))
            snake.setChasing(false)
        } else {
            snake.direction = characterToChase.x > snake.x
                ? HorizontalDirections.Right
                : HorizontalDirections.Left
            snake.move()
        }
    }
}

