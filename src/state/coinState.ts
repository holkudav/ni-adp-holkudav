import {CharacterState} from "./characterState";
import {WorldObject} from "./worldObject";
import {CollisionRect} from "../framework/collisions/model";
import {Collisions} from "../framework/collisions/collisions";
import {Asset} from "../framework/level";

export class CoinState extends WorldObject {
    get collisionBox(): CollisionRect {
        return Collisions.Instance.createCenterCollisionBox(this, this.width / 2 , this.height / 2 )
    }

    get predictedVX(): number {
        return 0;
    }

    get predictedVY(): number {
        return 0;
    }

    constructor(
        x: number,
        y: number,
        public width: number,
        public height: number,
        public pickedUp: boolean = false
    ) {
        super();
        this.x = x;
        this.y = y
    }

    update(characters: CharacterState[]) {
        this.pickedUp = this.pickedUp ||
            characters.some(it => {
                return this.isCollidingWith(it)
            })
    }

    static init(it: Asset) {
        return new CoinState(it.x, it.y, it.width, it.height)
    }
}
