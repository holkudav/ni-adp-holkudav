import {Collision, CollisionRect} from "../framework/collisions/model";
import {Level} from "../constants/maps";
import {Observer, Subject} from "../framework/observer";
import {Command, CommandReciever} from "../framework/command";
import {UserInput} from "../framework/inputs/userInput";
import {Collisions} from "../framework/collisions/collisions";
import {CharacterMode, HorizontalDirections, World} from "../constants/constants";
import {WorldObject} from "./worldObject";

export class CharacterState extends WorldObject implements Subject, CommandReciever {
    private observers: Observer[] = []
    private commands: Command[] = []

    mode: CharacterMode;
    direction: number;
    jump: number;
    standing: boolean;

    get collisionBox(): CollisionRect {
        return Collisions.Instance.createCenterCollisionBox(this, 35, 55);
    }

    get predictedVX(): number {
        return World.Character.Speed * this.direction;
    }

    get predictedVY(): number {
        return this.jump <= 0 ? World.Gravity : -World.Character.JumpSpeed;
    }

    private updateCharacterMoveDirection(input: UserInput) {
        if (this.mode === CharacterMode.Dead) {
            this.direction = 0
        }
        else if (input.getMoveRight()) {
            ({Right: this.direction} = HorizontalDirections);
        }
        else if (input.getMoveLeft()) {
            this.direction = HorizontalDirections.Left;
        }
        else {
            this.direction = 0;
        }
    }

    private static isCharacterMovingX(input: UserInput) {
        return input.getMoveLeft() || input.getMoveRight();
    }

    private isCharacterJumping() {
        return this.jump > 0;
    };

    private updateCharacterMode(
        movingX: boolean,
        jump: boolean,
    ) {
        if (this.mode === CharacterMode.Dead) {
            // nothing
        } else if (jump) {
            this.mode = CharacterMode.Jumping;
        } else if (!this.standing) {
            this.mode = CharacterMode.Falling;
        } else if (movingX) {
            this.mode = CharacterMode.Running;
        } else {
            this.mode = CharacterMode.Idle;
        }
    };

    private getCharacterJump(
        input: UserInput,
        collisions: Collision
    ) {
        if (input.getJump() && this.standing && this.mode !== CharacterMode.Dead) {
            this.mode = CharacterMode.Jumping
            this.observers.forEach(it => it.update(this))
            return World.Character.JumpSpeed;
        } else if (this.jump > 0 && this.jump < World.Character.JumpThreshold && collisions.vertical <= 0) {
            return World.Character.JumpThreshold - this.jump < World.Character.JumpSpeed
                ? World.Character.JumpThreshold
                : this.jump + World.Character.JumpSpeed;
        }
        return 0;
    };

    private updateCharacterVy(jumping: boolean, collisions: Collision) {
        this.vY = jumping
            ? -World.Character.JumpSpeed
            : World.Gravity + collisions.vertical;
    };

    private updateCharacterVx(
        movingX: boolean,
        collisions: Collision
    ) {
        if (this.mode === CharacterMode.Dead) {
            this.vX = 0
            return
        }
        this.vX = movingX
            ? this.direction * World.Character.Speed + collisions.horizontal
            : collisions.horizontal;
    };

    private updateStanding(collisions: Collision) {
        this.standing = collisions.vertical < 0;
    };

    die() {
        this.mode = CharacterMode.Dead;
        this.observers.forEach(it => it.update(this))
    }

    public update(
        input: UserInput,
        collisions: Collision,
    ) {
        this.processNextCommand()
        this.updateCharacterMoveDirection(input);

        const movingX = CharacterState.isCharacterMovingX(input);
        this.updateStanding(collisions);
        this.jump = this.getCharacterJump(input, collisions);
        const jumping = this.isCharacterJumping();
        this.updateCharacterVy(jumping, collisions);
        this.updateCharacterVx(movingX, collisions);
        this.updateCharacterMode(movingX, jumping);

        this.x = this.x + this.vX
        this.y = this.y + this.vY
    };

    constructor(properties: any) {
        super();
        this.x = properties.x | 0
        this.y = properties.y | 0
        this.vX = properties.vX | 0
        this.vY = properties.vY | 0
        this.direction = properties.direction | HorizontalDirections.Right
        this.mode = CharacterMode.Idle
    }

    static init(level: Level, cloneNumber: number): CharacterState {
        return new CharacterState({
            x: level.spawnPlaces[cloneNumber].x,
            y: level.spawnPlaces[cloneNumber].y,
        })
    }

    attach(observer: Observer): void {
        this.observers.push(observer)
    }

    detach(observer: Observer): void {
        this.observers = this.observers.filter(it => it !== observer)
    }

    enqueueCommand(command: Command): void {
        this.commands.push(command)
    }

    private processNextCommand() {
        this.commands.shift()?.execute()
    }
}
