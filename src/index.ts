import {initState} from "./state/state";
import {Level, LevelsManager} from "./constants/maps";
import {History} from "./state/history";
import {createPixiApp, getCanvasEl, initializeComponents, loadPixiAssets} from "./framework/game";
import {Textures} from "./constants/constants";

function createCanvas() {
  const container = getCanvasEl("container");
  const canvas = document.createElement('canvas', {})
  canvas.id = 'game'
  container.appendChild(canvas)
  return canvas
}

function subscribeToLevelButtons() {
  const ids = ['button1', 'button2', 'button3', 'button4', 'button5',]

  ids.forEach((it, i) => {
    document.getElementById(it)?.addEventListener('click', () => {
      document.getElementById('menu')!!.style.display = 'none'
      LevelsManager.levelNumber = i + 1
      newLevel(i + 1)
    })

  })
}

export async function newGame() {
  await loadPixiAssets({...Textures})
  newLevel(LevelsManager.levelNumber)
  subscribeToLevelButtons()
}
export function runNewIteration(level: Level) {
  History.Instance.initializeIteration()
  if (History.Instance.numberOfPastCharacters === level.spawnPlaces.length) {
    newLevel(LevelsManager.levelNumber)
  } else {
    initIteration(level)
  }
}

export function exitGame() {
  document.getElementById('menu')!!.style.display = 'block'
  LevelsManager.levelNumber = 1
  newGame()
}

export async function newLevel(levelNumber: number) {
  const level = await LevelsManager.getLevel(levelNumber)
  History.Instance.initializeLevel()

  initIteration(level)
}

export function initIteration(level: Level) {
  application?.destroy(true, {children: true})
  const canvasEl = createCanvas()
  application = createPixiApp(canvasEl, level);

  initializeComponents(application, initState(level));
  application.renderer.render(application.stage);
}

export let application: PIXI.Application

newGame()
