import PIXI from "pixi.js";
import {RenderFn} from "../framework/game";

export abstract class GameComponent {
    displayObject: PIXI.DisplayObject;
    render: RenderFn = () => { };
}
