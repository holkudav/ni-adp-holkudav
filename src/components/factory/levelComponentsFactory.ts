import {GameState} from "../../state/state";
import {CoinsComponent} from "../coinsComponent";
import {DoorComponent} from "../doorComponent";
import {PlatformComponent} from "../platformComponent";
import {SpawnPlacesComponent} from "../spawnPlacesComponent";
import {BackgroundComponent} from "../backgroundComponent";
import {GameComponent} from "../gameComponent";
import {StateComponent} from "../state";
import {History} from "../../state/history";
import {CharacterComponent} from "../characterComponent";
import * as PIXI from "pixi.js";
import {LavaComponent} from "../lavaComponent";
import {SnakesComponent} from "../snakeComponent";

export interface ComponentsFactory {
    getStateComponent(state: GameState): GameComponent
    getBackground(state: GameState): GameComponent
    getPlatform(state: GameState): GameComponent
    getCoins(state: GameState): GameComponent
    getGoal(state: GameState): GameComponent
    getSpawnPlaces(state: GameState): GameComponent
    getCharacters(state: GameState): GameComponent
    getLava(state: GameState): GameComponent
    getSnakes(state: GameState): GameComponent
}

export class LevelComponentsFactory implements ComponentsFactory{
    private static instance = new LevelComponentsFactory()

    static get Instance() {
        return this.instance
    }

    getBackground(state: GameState): GameComponent {
        return new BackgroundComponent(state)
    }

    getCoins(state: GameState): GameComponent {
        return new CoinsComponent(state);
    }

    getGoal(state: GameState): GameComponent {
        return new DoorComponent(state);
    }

    getPlatform(state: GameState): GameComponent {
        return new PlatformComponent(state);
    }

    getLava(state: GameState): GameComponent {
        return new LavaComponent(state)
    }

    getSpawnPlaces(state: GameState): GameComponent {
        return new SpawnPlacesComponent(state);
    }

    getStateComponent(): GameComponent {
        return new StateComponent();
    }

    getSnakes(state: GameState): GameComponent {
        return new SnakesComponent(state);
    }

    getCharacters(state: GameState): GameComponent {
        const container = new PIXI.Container()
        const renderFunctions: Function[] = []

        for (let i = 0; i <= History.Instance.numberOfPastCharacters; i++) {
            const character = new CharacterComponent(state, i)
            container.addChild(character.displayObject)
            renderFunctions.push(character.render)
        }

        return {
            displayObject: container,
            render: (_, state) => {
                renderFunctions.forEach(it => it(null, state))
            }
        }
    }

}
