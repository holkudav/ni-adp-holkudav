import * as PIXI from "pixi.js";
import {GameState} from "../state/state";
import {GameComponent} from "./gameComponent";
import {Textures} from "../constants/constants";

export class SpawnPlacesComponent extends GameComponent {
    constructor(state: GameState) {
        super();
        const resource = PIXI.Loader.shared.resources[Textures.SpawnPlaces];

        const container = new PIXI.Container();
        state.level.spawnPlaces.forEach(tile => {
            const sprite = new PIXI.Sprite(
                resource.textures![`place${tile.tileId - 61}.png`]
            );
            sprite.x = tile.x;
            sprite.y = tile.y;
            sprite.anchor = new PIXI.Point(0.5, 0)
            container.addChild(sprite);
        });

        this.displayObject = container
    }
}
