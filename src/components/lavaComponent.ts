import * as PIXI from "pixi.js";
import {GameState} from "../state/state";
import {GameComponent} from "./gameComponent";
import {Textures} from "../constants/constants";

export class LavaComponent extends GameComponent {
    constructor(state: GameState) {
        super();
        const resource = PIXI.Loader.shared.resources[Textures.Lava];

        const container = new PIXI.Container();
        state.level.lavaTiles!.forEach(tile => {
            const sprite = new PIXI.Sprite(
                resource.textures![`lava${tile.tileId - 1}.png`]
            );
            sprite.x = tile.x;
            sprite.y = tile.y;
            container.addChild(sprite);
        });
        this.displayObject = container
    }
}
