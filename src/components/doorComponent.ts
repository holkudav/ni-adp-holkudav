import * as PIXI from "pixi.js";
import {GameComponent} from "./gameComponent";
import {GameState} from "../state/state";
import {Textures} from "../constants/constants";

export class DoorComponent extends GameComponent {
    constructor(state: GameState) {
        super()
        const resource = PIXI.Loader.shared.resources[Textures.GoalDoor];
        const texture = resource.textures!["door0.png"];
        const sprite = new PIXI.Sprite(texture);
        sprite.x = state.level.goalDoor.x
        sprite.y = state.level.goalDoor.y - state.level.goalDoor.height
        this.displayObject = sprite
    }
}
