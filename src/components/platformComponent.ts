import * as PIXI from "pixi.js";
import {GameComponent} from "./gameComponent";
import {GameState} from "../state/state";
import {Textures} from "../constants/constants";

export class PlatformComponent extends GameComponent  {
    constructor(state: GameState) {
        super();
        const resource = PIXI.Loader.shared.resources[Textures.Platform];

        const container = new PIXI.Container();
        state.level.tiles!.forEach(tile => {
            const sprite = new PIXI.Sprite(resource.textures![`Tileset${tile.tileId - 1}.png`]);
            sprite.x = tile.x;
            sprite.y = tile.y;
            container.addChild(sprite);
        });
        this.displayObject = container
    }
}
