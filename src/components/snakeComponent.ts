import {GameState} from "../state/state";
import * as PIXI from "pixi.js";
import {SnakeState} from "../state/snakeState";
import {GameComponent} from "./gameComponent";
import {Textures} from "../constants/constants";

export class SnakesComponent extends GameComponent {
    constructor(state: GameState) {
        super()
        const resource = PIXI.Loader.shared.resources[Textures.Snake];
        const container = new PIXI.Container()
        const renderFunctions: Function[] = []

        state.states.snakes.forEach(it => {
            const sprite = new PIXI.AnimatedSprite(resource.spritesheet!.animations.walk);
            sprite.anchor = new PIXI.Point(0.5, 0.6);
            sprite.x = it.x;
            sprite.y = it.y;
            sprite.play();
            sprite.scale = new PIXI.Point(2.5, 2.5)
            sprite.animationSpeed = 0.2;
            container.addChild(sprite)
            renderFunctions.push((snake: SnakeState) => {
                sprite.x += snake.vX;
                sprite.y += snake.vY;
                sprite.scale.x = snake.direction
                    ? Math.abs(sprite.scale.x) * snake.direction
                    : sprite.scale.x;
                // if (snake.mode === SnakeMode.dead) {
                //     sprite.texture = resource.spritesheet!.animations.die
                //     sprite.loop = false
                // }
            })
        })
        this.displayObject = container
        this.render = (_: any, state: GameState) => {
            state.states.snakes.forEach((it, i) => {
                renderFunctions[i](it)
            })
        }
    }
}
