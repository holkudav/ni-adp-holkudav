import * as PIXI from "pixi.js";
import {GameState} from "../state/state";
import {GameComponent} from "./gameComponent";
import {Textures} from "../constants/constants";

/**
 * Creates background sprite.
 */
export class BackgroundComponent extends GameComponent {
  constructor(state: GameState) {
    super()
    const sceneHeight = state.level.levelHeight * state.level.tileHeight
    const sceneWidth = state.level.levelWidth * state.level.tileWidth
    const resource = PIXI.Loader.shared.resources[Textures.Background];
    const texture = resource.textures!["Background0.png"];
    const {width, height} = texture;
    const sprite = new PIXI.Sprite(texture);
    sprite.scale.x = sceneWidth / width;
    sprite.scale.y = sceneHeight / height;
    sprite.width = sceneWidth;
    sprite.height = sceneHeight;
    this.displayObject = sprite
  }

  displayObject: PIXI.DisplayObject;
}
