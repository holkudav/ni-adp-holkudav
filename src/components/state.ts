import * as PIXI from "pixi.js";
import {History} from "../state/history";
import {exitGame, runNewIteration} from "../index";
import {Input} from "../framework/inputs/input";
import {Camera} from "../state/camera/camera";
import {GameComponent} from "./gameComponent";
import {CharacterCollisions} from "../framework/collisions/model";
import {Collisions} from "../framework/collisions/collisions";

/**
 * Calculates new state on each frame.
 */
export class StateComponent extends GameComponent {
    constructor() {
        super();
        this.displayObject = new PIXI.Sprite()
        this.render = async (_, state) => {
            if (Input.Instace.getExit()) {
                exitGame()
            }
            if (Input.Instace.getClone()) {
                runNewIteration(state.level)
                return
            }
            History.Instance.tick()
            History.Instance.recordActions(Input.Instace.getSnapshot())
            const groupCollisions = Collisions.Instance.getCollisionsOfMovableGroup(state.states.characters, state.level.tiles!)

            state.states.characters.forEach((it, i) => {
                it.update(
                    History.Instance.getActions(i),
                    new CharacterCollisions(
                        Collisions.Instance.getCollisionWithNonMovables(it, state.level.tiles),
                        groupCollisions[i]
                    )
                )
            })
            state.states.coins.forEach(it => {
                it.update(state.states.characters)
            })
            state.states.snakes.forEach(it => {
                it.update(state.states.characters, state.level.tiles);
            })
            await state.states.goal.update(state.states.characters)

            Camera.Instance.update(state.states.characters[state.states.characters.length - 1])
        };
    }
}
