import * as PIXI from "pixi.js";
import {GameState} from "../state/state";
import {GameComponent} from "./gameComponent";
import {Textures} from "../constants/constants";

export class CoinsComponent extends GameComponent {
    constructor(state: GameState) {
        super();
        const container = new PIXI.Container()
        const renderFunctions: Function[] = []

        const resource = PIXI.Loader.shared.resources[Textures.Coin];
        state.level.coins.forEach((it, i) => {
            const sprite = new PIXI.AnimatedSprite(resource.spritesheet!.animations.spin);
            sprite.x = it.x
            sprite.y = it.y - it.height
            sprite.scale = new PIXI.Point(1.25, 1.25)
            sprite.play();
            sprite.animationSpeed = 0.2;
            container.addChild(sprite)
            renderFunctions.push((_: any, state: GameState) => {
                if (state.states.coins[i].pickedUp && sprite.textures !== resource.spritesheet!.animations.pickUp) {
                    sprite.textures = resource.spritesheet!.animations.pickUp;
                    sprite.loop = false
                    sprite.play();
                }
            })
        })

        this.displayObject = container
        this.render = (_, state) => {
            renderFunctions.forEach((it, i) => it(null, state))
        }
    }
}
