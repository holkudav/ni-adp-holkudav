import * as PIXI from "pixi.js";
import {AnimatedSprite} from "pixi.js";
import {GameState} from "../state/state";
import {History} from "../state/history";
import {GameComponent} from "./gameComponent";
import {CharacterMode, Textures} from "../constants/constants";
import {RenderFn} from "../framework/game";

const CharacterTextures = {
  [CharacterMode.Idle]: "idle",
  [CharacterMode.Running]: "walk",
  [CharacterMode.Jumping]: "jump",
  [CharacterMode.Falling]: "fall",
  [CharacterMode.Dead]: "die",
};

export class CharacterComponent extends GameComponent {
  constructor(state: GameState, cloneNumber: number = History.Instance.numberOfPastCharacters) {
    super()
    const texture = cloneNumber === History.Instance.numberOfPastCharacters ? Textures.Character : Textures.PastCharacter
    const resource = PIXI.Loader.shared.resources[texture];
    const sprite = new PIXI.AnimatedSprite(resource.spritesheet!.animations.idle);
    sprite.anchor = new PIXI.Point(0.5, 0.5);
    sprite.x = state.level.spawnPlaces[cloneNumber].x;
    sprite.y = state.level.spawnPlaces[cloneNumber].y;
    sprite.scale = new PIXI.Point(3, 3);
    sprite.play();
    sprite.animationSpeed = 0.2;
    this.displayObject = sprite
    this.render = this.renderClone(sprite, cloneNumber, resource)
  }

  renderClone(sprite: AnimatedSprite, cloneNumber: number, resource: any): RenderFn {
    return function (_, state: GameState) {
      const character = state.states.characters[cloneNumber]
      sprite.scale.x = character.direction
          ? Math.abs(sprite.scale.x) * character.direction
          : sprite.scale.x;

      sprite.x += character.vX;
      sprite.y += character.vY;

      resource.spritesheet!.animations;
      const currentAnimation = CharacterTextures[character.mode];
      const currentTextures = resource.spritesheet!.animations[currentAnimation];
      if (sprite.textures !== currentTextures && currentTextures) {
        sprite.textures = currentTextures;
        sprite.loop = character.mode != CharacterMode.Dead
        sprite.play();
      }
    }
  }
}
