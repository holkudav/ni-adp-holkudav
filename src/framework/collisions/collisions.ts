import {WorldObject} from "../../state/worldObject";
import {Collision, CollisionRect} from "./model";
import {CollisionOfMovables} from "./collisionOfMovables";

export class Collisions {
    private static instance = new Collisions()

    static get Instance() {
        return this.instance
    }

    createCornerCollisionBox(object: WorldObject, width: number, height: number): CollisionRect {
        return {
            x: object.x,
            y: object.y,
            width: width,
            height: height
        };
    }

    createCenterCollisionBox(object: WorldObject, width: number, height: number): CollisionRect {
        return {
            x: object.x - width / 2,
            y: object.y - height / 2,
            width: width,
            height: height
        };
    }

    private adjustCollisionBox(box: CollisionRect, vX: number = 0, vY: number = 0): CollisionRect {
        return {
            ...box,
            x: box.x + vX,
            y: box.y + vY
        }
    }

    joinBoxes(b1: CollisionRect, b2: CollisionRect) {
        const left = b1.x < b2.x ? b1 : b2
        const right = b1.x >= b2.x ? b1 : b2
        const up = b1.y < b2.y ? b1 : b2
        const down = b1.y >= b2.y ? b1 : b2

        return {
            x: left.x,
            y: up.y,
            width: right.x + right.width - left.x,
            height: down.y + down.height - up.y
        };
    }

    areColliding(rect1: CollisionRect, rect2: CollisionRect): boolean {
        return (
            rect1.x + rect1.width > rect2.x &&
            rect1.x < rect2.x + rect2.width &&
            rect1.y + rect1.height > rect2.y &&
            rect1.y < rect2.y + rect2.height
        );
    }

    getHorizontalCollision(
        a: WorldObject,
        b: WorldObject,
    ) {
        const adjustedHRect1 = Collisions.Instance.adjustCollisionBox(a.collisionBox, a.predictedVX)
        const adjustedHRect2 = Collisions.Instance.adjustCollisionBox(b.collisionBox, b.predictedVX)

        if (Collisions.Instance.areColliding(adjustedHRect1, adjustedHRect2)) {
            return a.predictedVX >= 0
                ? adjustedHRect2.x - (adjustedHRect1.x + adjustedHRect1.width)
                : adjustedHRect2.x + adjustedHRect2.width - adjustedHRect1.x;
        }
        return 0;
    };

    getVerticalCollision (
        a: WorldObject,
        b: WorldObject,
    ) {
        const adjustedYRect1 = Collisions.Instance.adjustCollisionBox(a.collisionBox, 0, a.predictedVY)
        const adjustedYRect2 = Collisions.Instance.adjustCollisionBox(b.collisionBox, 0, b.predictedVY)

        if (Collisions.Instance.areColliding(adjustedYRect1, adjustedYRect2)) {
            return a.predictedVY >= 0
                ? adjustedYRect2.y - (adjustedYRect1.y + adjustedYRect1.height)
                : adjustedYRect2.y + adjustedYRect2.height - adjustedYRect1.y;
        }
        return 0;
    };

    getDiagonalCollision = (
        a: WorldObject,
        b: WorldObject,
    ) => {
        const adjustedRect1 = Collisions.Instance.adjustCollisionBox(a.collisionBox, a.predictedVX, a.predictedVY)
        const adjustedRect2 = Collisions.Instance.adjustCollisionBox(b.collisionBox, b.predictedVX, b.predictedVY)

        const NE = a.vX >= 0 && a.vY <= 0;
        const NW = a.vX <= 0 && a.vY <= 0;
        const SE = a.vX >= 0 && a.vY >= 0;
        const SW = a.vX <= 0 && a.vY >= 0;

        if (Collisions.Instance.areColliding(adjustedRect1, adjustedRect2)) {
            if (NE) {
                return {
                    h: adjustedRect2.x - (adjustedRect1.x + adjustedRect1.width),
                    v: adjustedRect2.y + adjustedRect2.height - adjustedRect1.y
                };
            } else if (NW) {
                return {
                    h: adjustedRect2.x + adjustedRect2.width - adjustedRect1.x,
                    v: adjustedRect2.y + adjustedRect2.height - adjustedRect1.y
                };
            } else if (SE) {
                return {
                    h: adjustedRect2.x - (adjustedRect1.x + adjustedRect1.width),
                    v: adjustedRect2.y - (adjustedRect1.y + adjustedRect1.height)
                };
            } else if (SW) {
                return {
                    h: adjustedRect2.x + adjustedRect2.width - adjustedRect1.x,
                    v: adjustedRect2.y - (adjustedRect1.y + adjustedRect1.height)
                };
            }
        }
        return {
            h: 0,
            v: 0
        };
    };

    getCollisionWithNonMovables(object: WorldObject, nonMovables: WorldObject[]): Collision {
        const collisions = nonMovables.map(it => ({
            h: Collisions.Instance.getHorizontalCollision(object, it),
            v: Collisions.Instance.getVerticalCollision(object, it),
            diag: Collisions.Instance.getDiagonalCollision(object, it)
        }));

        const collisionHorizontal = collisions.find(collision => collision.h);
        const collisionVertical = collisions.find(collision => collision.v);
        const collisionDiag = collisions.find(collision => collision.diag.h || collision.diag.v);

        return new Collision(collisionVertical?.v, collisionHorizontal?.h, collisionDiag?.diag)
    }


    getCollisionsOfMovableGroup(objects: WorldObject[], nonMovables: WorldObject[]): Collision[] {
        return new CollisionOfMovables().getCollisionsOfMovableGroup(objects, nonMovables)
    }
}
