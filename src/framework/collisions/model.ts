export class Collision {
    constructor(
        private v?: number,
        private h?: number,
        private d?: { v: number, h: number },
    ) { }

    get horizontal(): number {
        if (this.h || this.v) {
            return this.h || 0
        }

        return this.d?.h || 0
    }

    get vertical(): number {
        if (this.h || this.v) {
            return this.v || 0
        }

        return this.d?.v || 0
    }
}

export class CharacterCollisions extends Collision {
    constructor(private platform: Collision, private character: Collision) {
        super(0, 0, {v: 0, h: 0});
    }

    get horizontal(): number {
        return this.platform.horizontal + this.character.horizontal
    }

    get vertical(): number {
        return this.platform.vertical + this.character.vertical
    }
}

export interface CollisionRect {
    x: number;
    y: number;
    width: number;
    height: number;
}
