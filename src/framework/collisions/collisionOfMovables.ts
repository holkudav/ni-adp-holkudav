import {Collision, CollisionRect} from "./model";
import {Collisions} from "./collisions";
import {WorldObject} from "../../state/worldObject";

export class CollisionOfMovables {
    getCollisionsOfMovableGroup(objects: WorldObject[], nonMovables: WorldObject[]): Collision[] {
        const horizontal = this.resolveHorizontalCollisions(objects, nonMovables)
        const vertical = this.resolveVerticalCollisions(objects, nonMovables)

        return objects.map((_,  i) => {
            return new Collision(vertical[i], horizontal[i])
        })
    }

    // not working properly
    private resolveVerticalCollisions(objects: WorldObject[], nonMovables: WorldObject[]) {
        let group: Movable[] = objects
            .map((it, i) => new Movable(i, it))
            .sort((a, b) => a.y < b.y ? -1 : 1)

        const processed: number[] = []

        // from the bottom -> up
        for (let i = 0; i < group.length; i++) {
            for (let j = i + 1; j < group.length; j++) {
                const below = group[j].copy()
                const above = group[i]
                const originalVy = below.predictedVY

                if (processed.includes(above.index) || !above.isCollidingWith(below)) {
                    continue
                }

                below.predictedVy = 0
                const collision = above.collisionWith(below).vertical
                above.y += collision
                if (originalVy < 0) {
                    above.y += originalVy
                }
                processed.push(above.index)
            }
        }

        // from top -> bottom
        group = group.map(it => {
            const collision = Collisions.Instance.getCollisionWithNonMovables(it, nonMovables).vertical
            const ceiling = collision > 0 ? collision : 0
            it.predictedVy = ceiling
            return it
        }).sort((a, b) => a.y > b.y ? -1 : 1)
        for (let i = 0; i < group.length; i++) {
            for (let j = i + 1; j < group.length; j++) {
                const below = group[i]
                const above = group[j]

                if (above.isCollidingWith(below)) {
                    below.predictedVy = above.predictedVy
                }
            }
        }
        group.forEach(it => it.y += it.predictedVy)

        return group
            .sort((a, b) => a.index < b.index ? -1 : 1)
            .map(it => it.y - objects[it.index].y)
    }

    private resolveHorizontalCollisions(objects: WorldObject[], nonMovables: WorldObject[]): number[] {
        const collidingGroups: HorizontallyMoveableGroup[] = []
        let group: Movable[] = objects.map((it, i) => new Movable(i, it))
        while (group.length) {
            const colliding = new HorizontallyMoveableGroup()
            for (let _ of group) {
                for (let it of group) {
                    colliding.add(it)
                }
            }
            colliding.adjustToNonMovables(nonMovables)
            collidingGroups.push(colliding)
            group = group.filter(it => !colliding.members.find(c => c.index === it.index))
        }
        return collidingGroups
            .map(it => it.members)
            .reduce((a, b) => a.concat(b))
            .map(it => it.x - objects[it.index].x)
    }
}

export class Movable extends WorldObject {
    public predictedVx: number;
    public predictedVy: number;
    collisionBox: CollisionRect

    constructor(
        public index: number,
        object: WorldObject
    ) {
        super();
        this.x = object.x
        this.y = object.y
        this.predictedVx = object.predictedVX
        this.predictedVy = object.predictedVY
        this.collisionBox = object.collisionBox
    }

    copy(): Movable {
        return {...this}
    }

    moveHorizontally(n: number) {
        this.x += n
        this.collisionBox.x += n
    }

    moveVertically(n: number) {
        this.y += n
        this.collisionBox.y += n
    }

    get predictedVX(): number {
        return this.predictedVx;
    }

    get predictedVY(): number {
        return this.predictedVy;
    }
}

class HorizontallyMoveableGroup extends WorldObject {
    public members: Movable[] = []
    private box: CollisionRect;

    get mass() {
        return this.members.length
    }

    add(other: Movable) {
        if (!this.members.length) {
            this.box = other.collisionBox
            this.members.push(other)
            return
        }

        const collision = this.box.x < other.x
            ? this.collisionWith(other).horizontal
            : - other.collisionWith(this).horizontal
        if (!collision || this.members.find(it => it.index == other.index)) {
            return
        }

        const velocity = collision / 2

        other.moveHorizontally(- velocity)
        this.members.forEach(it => it.moveHorizontally(velocity))
        this.members.push(other)
        this.box = Collisions.Instance.joinBoxes(this.box, other.collisionBox)
    }

    adjustToNonMovables(nonMovables: WorldObject[]) {
        const platformCollision = this.members
            .map(it => Collisions.Instance.getCollisionWithNonMovables(it, nonMovables).horizontal)
            .find(it => it !== 0)

        if (platformCollision) {
            this.members.forEach(it => it.x += Math.abs(platformCollision) > it.predictedVX ? -(platformCollision) % it.predictedVX : platformCollision)
        }
    }

    get collisionBox(): CollisionRect {
        return this.box;
    }

    get predictedVX(): number {
        return 0;
    }

    get predictedVY(): number {
        return 0;
    }
}

