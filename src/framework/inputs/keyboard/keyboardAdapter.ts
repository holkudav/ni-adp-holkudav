import {UserInput} from "../userInput";
import {Keyboard} from "./keyboard";

export class KeyboardAdapter implements UserInput {
    private jumpKey = "ArrowUp"
    private leftKey = "ArrowLeft"
    private rightKey = "ArrowRight"
    private newIterationKey = "KeyL"
    private pauseKey = "KeyE"

    constructor(private keyboard: Keyboard) { }

    getClone(): boolean {
        return this.keyboard.isPressed(this.newIterationKey);
    }

    getJump(): boolean {
        return this.keyboard.isPressed(this.jumpKey);
    }

    getMoveLeft(): boolean {
        return this.keyboard.isPressed(this.leftKey);
    }

    getMoveRight(): boolean {
        return this.keyboard.isPressed(this.rightKey);
    }

    getExit(): boolean {
        return this.keyboard.isPressed(this.pauseKey);
    }

}
