export class Keyboard {
    private pressedKeys: any = {}

    constructor() {
        this.subscribeToKeyboardEvents()
    }

    isPressed(keyCode: string): boolean {
        return this.pressedKeys[keyCode]
    }

    private subscribeToKeyboardEvents() {
        document.addEventListener("keyup", (e: KeyboardEvent) => {
            this.pressedKeys[e.code] = false;
        });
        document.addEventListener("keydown", (e: KeyboardEvent) => {
            this.pressedKeys[e.code] = true;
        });
    }
}
