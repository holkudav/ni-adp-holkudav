import {KeyboardAdapter} from "./keyboard/keyboardAdapter";
import {UserInput} from "./userInput";
import {Keyboard} from "./keyboard/keyboard";

class Snapshot implements UserInput {
    constructor(
        private jump: boolean,
        private moveLeft: boolean,
        private moveRight: boolean
    ) { }

    getClone(): boolean {
        return false;
    }

    getExit(): boolean {
        return false;
    }

    getJump(): boolean {
        return this.jump;
    }

    getMoveLeft(): boolean {
        return this.moveLeft;
    }

    getMoveRight(): boolean {
        return this.moveRight;
    }
}

export class Input {
    defaultSnapshot: UserInput

    // One line of code to change the input methods
    // private static instance: Input = new Input(new Controller())
    // private static instance: Input = new Input(new JoyStick())
    // ...
    private static instance: Input = new Input(new KeyboardAdapter(new Keyboard()))

    static get Instace() {
        return this.instance
    }

    constructor(private userInput: UserInput) {
        this.defaultSnapshot = new Snapshot(false, false, false)
    }

    getClone(): boolean {
        return this.userInput.getClone();
    }

    getExit(): boolean {
        return this.userInput.getExit();
    }

    getSnapshot(): UserInput {
        return new Snapshot(
            this.userInput.getJump(),
            this.userInput.getMoveLeft(),
            this.userInput.getMoveRight(),
        )
    }

    getDefault(): UserInput {
        return this.defaultSnapshot;
    }
}
