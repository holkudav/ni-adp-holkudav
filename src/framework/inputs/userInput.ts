export interface UserInput {
    getMoveLeft(): boolean
    getMoveRight(): boolean
    getJump(): boolean
    getClone(): boolean
    getExit(): boolean
    // getRestart(): boolean
    // getExit(): boolean
}
