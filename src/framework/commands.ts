import {CharacterState} from "../state/characterState";
import {Command} from "./command";

export class DieCommand implements Command {
    constructor(private reciever: CharacterState) { }

    execute(): void {
        this.reciever.die()
    }
}
