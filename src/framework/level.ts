import {WorldObject} from "../state/worldObject"
import {Collisions} from "./collisions/collisions";
import {CollisionRect} from "./collisions/model";

export interface RawLevel {
  layers: any[];
  height: number;
  width: number;
  tileheight: number;
  tilewidth: number;
}

export class Asset extends WorldObject {

  constructor(
      public id: string,
      public x: number,
      public y: number,
      public width: number,
      public height: number,
) {
    super();
  }

  get collisionBox(): CollisionRect {
    return Collisions.Instance.createCornerCollisionBox(this, this.width, this.height);
  }

  get predictedVX(): number {
    return 0;
  }

  get predictedVY(): number {
    return 0;
  }
}

export class ParsedTile extends Asset{

  constructor(
      public tileId: number,
      id: string,
      x: number,
      y: number,
      width: number,
      height: number,
  ) {
    super(id, x, y, width, height);
  }
}

export class ParsedLevel {
  snakes: Asset[]
  coins: Asset[]
  goalDoor: Asset
  spawnPlaces: ParsedTile[]
  tiles: ParsedTile[]
  lavaTiles: ParsedTile[]
  tileWidth: number
  tileHeight: number
  levelWidth: number
  levelHeight: number
}

export enum TileId {
  Blank = 0
}

/**
 * Parses json exported from Tiled.
 * Level data comes as a 1-dimensional array. However, we need to establish things like tile's position in 2d world.
 * Later, we will also need to know if a tile has neighbours. Therefore, let's transform it into 2d array.
 *
 * Once all required data is calculated, flatten array back into 1d and filter all blank tiles.
 *
 * @param rawLevel json file exported from Tiled
 */
export const createLevel = (rawLevel: RawLevel): ParsedLevel => {
  return {
    spawnPlaces: createSpawnPlaces(rawLevel),
    tiles: createPlatform(rawLevel),
    lavaTiles: createLava(rawLevel),
    coins: createCoins(rawLevel),
    snakes: createSnakes(rawLevel),
    goalDoor: createGoalDoor(rawLevel),
    tileWidth: rawLevel.tilewidth,
    tileHeight: rawLevel.tileheight,
    levelWidth:rawLevel.width,
    levelHeight: rawLevel.height
  }
};

const createPlatform = (rawLevel: RawLevel): ParsedTile[] => {
  const rawTiles = rawLevel.layers.filter(it => it.name === 'TileLayer')[0]
  return parseTileLayer(rawTiles, rawLevel.tilewidth, rawLevel.tileheight)
}

const createLava = (rawLevel: RawLevel): ParsedTile[] => {
  const rawTiles = rawLevel.layers.filter(it => it.name === 'LavaLayer')[0]
  if (!rawTiles) return []
  return parseTileLayer(rawTiles, rawLevel.tilewidth, rawLevel.tileheight)
}

const createSpawnPlaces = (rawLevel: RawLevel): ParsedTile[] => {
  const rawTiles = rawLevel.layers.filter(it => it.name === 'SpawnPlaces')[0]
  return parseTileLayer(rawTiles, rawLevel.tilewidth, rawLevel.tileheight)
      .sort((a, b) => a.tileId < b.tileId ? -1 : 1)
}

const createSnakes = (rawLevel: RawLevel): Asset[] => {
  return parseLayerOfSameObjects(rawLevel, 'Snakes')
}

const createCoins = (rawLevel: RawLevel): Asset[] => {
  return parseLayerOfSameObjects(rawLevel, 'Coins')
}

const createGoalDoor = (rawLevel: RawLevel): Asset => {
  return parseLayerOfSameObjects(rawLevel, 'EndDoor')[0]
}

const parseTileLayer = (layer: any, tileWidth: number, tileHeight: number) => {
  const array2d = Array.from({ length: layer.height }).map((_, i) =>
      layer.data.slice(i * layer.width, (i + 1) * layer.width)
  );
  return array2d
      .map((row, i) => {
        return row.map((tileId: number, j: number) => {
          return new ParsedTile(
              tileId,
              `${i}_${j}`,
              tileWidth * j,
              tileHeight * i,
              tileWidth,
              tileHeight,
          )
        });
      })
      .reduce((acc, row) => row.concat(acc), [])
      .filter((tile:any) => tile.tileId !== TileId.Blank);
}

function parseLayerOfSameObjects (rawLevel: RawLevel, layerName: string): Asset[] {
  return rawLevel.layers
      .filter(it => it.name === layerName)
      [0]?.objects
      .map((it: any, i: number) => {
        return new Asset(
            i.toString(),
            it.x,
            it.y,
            it.width,
            it.height,
        )
      }) || []

}
