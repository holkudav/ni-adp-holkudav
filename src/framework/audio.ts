import {Observer} from "./observer";
import {CharacterState} from "../state/characterState";
import * as Sound from 'pixi-sound';
import * as PIXI from 'pixi.js';
import {CharacterMode} from "../constants/constants";


export class CharacterAudio implements Observer {

    update(character: any): void {
        const mode = character.mode
        Sound.default.find(mode)?.play({speed: 1.1, loop: false, })
        if (mode == CharacterMode.Dead) {
            character.detach(this)
        }
    }

    constructor(characters: CharacterState[]) {
        this.loadSounds()
        characters.forEach(it => it.attach(this))
    }

    private loadSounds() {
        const pixiLoader = new PIXI.Loader()
        pixiLoader.add('Jumping', 'assets/sound/Jump.wav')
        pixiLoader.add('Dead', 'assets/sound/Dead.wav')
        pixiLoader.load()
    }

    static attachTo(characters: CharacterState[]) {
        new CharacterAudio(characters)
    }
}
