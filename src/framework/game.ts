import * as PIXI from "pixi.js";
import {Level} from "../constants/maps";
import {GameState} from "../state/state";
import {Camera} from "../state/camera/camera";
import {LevelComponentsFactory} from "../components/factory/levelComponentsFactory";
import {Viewport} from "pixi-viewport";

export type RenderFn = (displayObject: PIXI.DisplayObject, state: GameState) => void;

export function initializeComponents (
    app: PIXI.Application,
    state: GameState
) {
  const components = [
    LevelComponentsFactory.Instance.getStateComponent(),
    LevelComponentsFactory.Instance.getBackground(state),
    LevelComponentsFactory.Instance.getPlatform(state),
    LevelComponentsFactory.Instance.getSpawnPlaces(state),
    LevelComponentsFactory.Instance.getGoal(state),
    LevelComponentsFactory.Instance.getCoins(state),
    LevelComponentsFactory.Instance.getSnakes(state),
    LevelComponentsFactory.Instance.getCharacters(state)
  ]
  components.forEach(it => {
    Camera.Instance.getViewPort().addChild(it.displayObject);
    app.ticker.add(() => {
      it.render(it.displayObject, state);
    });
  });
}

/**
 * Gets canvas element from DOM.
 * Throws error in case it cannot be found.
 *
 * @param id id of canvas element
 */
export const getCanvasEl = (id: string) => {
  const canvas = document.getElementById(id) as HTMLCanvasElement | null;
  if (!canvas) {
    throw new Error(`Canvas with specified id ${id} not found.`);
  }
  return canvas;
};

export const createPixiApp = (canvas: HTMLCanvasElement, level: Level) => {
  canvas.height = level.tileWidth * level.viewWidth;
  canvas.width = level.tileHeight * level.viewHeight;

  const config = {
    view: canvas,
    width: level.tileWidth * level.viewWidth,
    height: level.tileHeight * level.viewHeight,
  }
  const app = new PIXI.Application(config);

  Camera.Instance.setViewPort(new Viewport({
    screenWidth: canvas.width,
    screenHeight: canvas.height,
    worldWidth: level.tileWidth * level.levelWidth,
    worldHeight: level.tileHeight * level.levelHeight,

    // the interaction module is important for wheel to work properly when renderer.view is placed or scaled
    interaction: app.renderer.plugins.interaction
  }))
  Camera.Instance.moveCornerTo(level.tileWidth, level.tileHeight)
  Camera.Instance.setMode(level.camera)
  app.stage.addChild(Camera.Instance.getViewPort())

  return app;
};

/**
 * Promisified version of PIXI Loader.
 * Loads all required assets.
 *
 * @param textures map of textures to their paths
 */
export const loadPixiAssets = (textures: { [key: string]: string }) => {
  return new Promise(resolve => {
    PIXI.Loader.shared.add(
      Object.keys(textures).map((key: keyof typeof textures) => textures[key])
    );
    PIXI.Loader.shared.load(resolve);
  });
};
