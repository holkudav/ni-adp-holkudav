export interface Command {
    execute(): void
}

export interface CommandReciever {
    enqueueCommand(command: Command): void
}
